# Scala.js project prototype
[![Scala.js](https://www.scala-js.org/assets/badges/scalajs-0.6.15.svg)](https://www.scala-js.org)

---

## Run the server locally in a Docker container 
 1. Install [SBT](http://www.scala-sbt.org/) and [Docker](https://docs.docker.com/engine/getstarted/step_one/) 
 2. Start SBT in the project's root directory and switch to the JVM project: `project appJVM`
 3. Execute `dockerComposeUp` in SBT to build the application, package it as a Docker image, and run the created image 
 4. Point your browser to `localhost` 
 5. Stop the running containers using `dockerComposeStop`

## Project structure
 * Backend code is in `app/jvm`. This includes code for the web server and connecting to the database. It will run on the JVM.
 * Frontend code is in `app/js`. This code will be translated to JavaScript and run in the client's browser.
 * The code shared between frontend and backend is in `app/shared`.

## Backend
 * [Scala](https://www.scala-lang.org/): Implements the server logic
 * [http4s](http://http4s.org/): Provides the HTTP routing for the server

## Frontend
 * [Scala.js](https://www.scala-js.org/): Compiles Scala to JavaScript. With the compiled Scala code, we add GUI elements dynamically, read their contents, and make Ajax calls to the server 
 * [Pure.css](https://purecss.io/): Styles GUI elements such as buttons, tables, and forms in the browser
 