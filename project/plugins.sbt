// [scalajs 1.0] addSbtPlugin("org.scala-js" % "sbt-scalajs" % "1.0.0-M1")
// [scalajs 1.0] addSbtPlugin("org.scala-js" % "sbt-jsdependencies" % "1.0.0-M1")

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.21")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.1")

// Creates a Docker image from the jar: https://github.com/marcuslonnberg/sbt-docker
addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.5.0")

// Lets us use docker compose from SBT: sbt; project appJVM; dockerComposeUp
// https://github.com/Tapad/sbt-docker-compose
addSbtPlugin("com.tapad" % "sbt-docker-compose" % "1.0.30")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.0")