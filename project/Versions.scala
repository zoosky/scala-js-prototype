object Versions {
  val http4s = "0.18.0-M5"

  val scalaTags = "0.6.7"
  val scalaJsDom = "0.9.3"
  val scalaJsJquery = "0.9.2"
  val jquery = "3.2.1"

  val config = "1.3.1"
  val log4j = "2.9.1"
  val scalatest = "3.0.4"
}